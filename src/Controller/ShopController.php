<?php

namespace App\Controller;

use App\Entity\Processor;
use App\Entity\ProcessorStatistics;
use App\Repository\CategoriesRepository;
use App\Repository\ProcessorBrandsRepository;
use App\Repository\ProcessorIntegratedGraphicsRepository;
use App\Repository\ProcessorRepository;
use App\Repository\ProcessorSocketTypesRepository;
use App\Repository\ProcessorStatisticsRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    #[Route('/shop', name: 'shop', methods:['GET', 'HEAD'])]
    public function index(ManagerRegistry $doctrine,
                          Request $request,
                          ProcessorRepository $processorRepository,
                          ProcessorBrandsRepository $processorBrandsRepository,
                          ProcessorSocketTypesRepository $processorSocketTypesRepository,
                          CategoriesRepository $categoriesRepository,
                          ProcessorIntegratedGraphicsRepository $integratedGraphicsRepository,
                          ProcessorStatisticsRepository $processorStatisticsRepository)
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');
        $brandFilter = $request->query->all()['brandFilter'] ?? null;
        $brands = $processorBrandsRepository->findAll();
        $graphicsFilter = $request->query->all()['graphicsFilter'] ?? null;
        $integratedGraphics = $integratedGraphicsRepository->findAll();
        $coresFilter = $request->query->all()['coresFilter'] ?? null;
        $allProcessors = $processorRepository->findAll();
        $allStatistics = $processorStatisticsRepository->findAll();

        foreach ($allProcessors as $processor) {
            $processorId = $processor->getId();
            $existingStatistic = array_filter($allStatistics, function ($statistic) use ($processorId) {
                return $statistic->getProcessorId()->getId() === $processorId;
            });

            if (empty($existingStatistic)) {
                $newStatistic = new ProcessorStatistics();
                $newStatistic->setProcessorId($processor);
                $entityManager->persist($newStatistic);
            }
        }
        $entityManager->flush();

        $cores = array_unique(array_map(function ($processor) {
                return $processor->getCores();
            }, $allProcessors)
        );
        sort($cores);
        $socketFilter = $request->query->all()['socketFilter'] ?? null;
        $sockets = $processorSocketTypesRepository->findAll();
        $categoryFilter = $request->query->all()['categoryFilter'] ?? null;
        $categories = $categoriesRepository->findAll();

        $cacheMemoryFilter = $request->query->all()['cacheMemoryFilter'] ?? null;
        $cache = array_unique(array_map(function ($processor) {
                return $processor->getCacheMemory();
            }, $allProcessors)
        );
        sort($cache);

        $techProcessFilter = $request->query->all()['techProcessFilter'] ?? null;
        $techProcess = array_unique(array_map(function ($processor) {
                return $processor->getTechProcess();
            }, $allProcessors)
        );
        sort($techProcess);

        $tdpFilter = $request->query->all()['tdpFilter'] ?? null;
        $tdp = array_unique(array_map(function ($processor) {
                return $processor->getTdp();
            }, $allProcessors)
        );
        sort($tdp);

        $minPrice = $request->query->get('minPrice', null);
        $maxPrice = $request->query->get('maxPrice', null);

        $processors = $entityManager->getRepository(Processor::class)->findAllProcessor(
            $field,
            $direction,
            $searchQuery,
            $brandFilter,
            $graphicsFilter,
            $coresFilter,
            $socketFilter,
            $categoryFilter,
            $cacheMemoryFilter,
            $techProcessFilter,
            $tdpFilter,
            $minPrice,
            $maxPrice
        );

        return $this->render('shop/index.html.twig', [
            'processors' => $processors,
            'sortBy' => $sortBy,
            'searchQuery' => $searchQuery,
            'brandFilter' => $brandFilter,
            'brands' => $brands,
            'graphicsFilter' => $graphicsFilter,
            'integratedGraphics' => $integratedGraphics,
            'coresFilter' => $coresFilter,
            'cores' => $cores,
            'socketFilter' => $socketFilter,
            'sockets' => $sockets,
            'categoryFilter' => $categoryFilter,
            'categories' => $categories,
            'cacheMemoryFilter' => $cacheMemoryFilter,
            'cache' => $cache,
            'techProcessFilter' => $techProcessFilter,
            'techProcess' => $techProcess,
            'tdpFilter' => $tdpFilter,
            'tdp' => $tdp,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
        ]);
    }
    #[Route('/aboutUs', name: 'about_us')]
    public function aboutUs(): Response
    {
        return $this->render('aboutUs/index.html.twig');
    }
    #[Route('/contacts', name: 'contacts')]
    public function contacts(): Response
    {
        return $this->render('contacts/index.html.twig');
    }
}
