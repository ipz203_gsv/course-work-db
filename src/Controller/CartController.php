<?php

namespace App\Controller;

use App\Entity\OrderDetails;
use App\Entity\Orders;
use App\Entity\Processor;
use App\Entity\ProcessorStatistics;
use App\Form\OrderFormType;
use App\Repository\CustomersRepository;
use App\Repository\ProcessorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    #[Route('/cart', name: 'view_cart')]
    public function viewCart(SessionInterface $sessionInterface, ProcessorRepository $processorRepository): Response
    {
        $ProcessorID = $sessionInterface->get('cart', []);

        $Processors = $processorRepository->findBy(['id' => $ProcessorID]);

        $cartData = [];
        $productQuantities = array_count_values($ProcessorID);
        $totalPrice = 0;

        foreach ($Processors as $processor) {
            $processorId = $processor->getId();
            $cartData[] = [
                'id' => $processorId,
                'image' => $processor->getImage(),
                'name' => $processor->getName(),
                'price' => $processor->getPrice(),
                'quantity' => $productQuantities[$processorId],
            ];
            $totalPrice += $processor->getPrice() * $productQuantities[$processorId];
        }


        return $this->render('cart/index.html.twig', [
            'cartData' => $cartData,
            'totalPrice' => $totalPrice,
        ]);
    }

    //Додавання у кошик
    #[Route('/add-to-cart/{id}', name: 'add_to_cart')]
    public function addToCart(SessionInterface $sessionInterface, EntityManagerInterface $entityManager, $id): Response
    {
        $processor = $entityManager->getRepository(Processor::class)->find($id);

        if ($processor->getStockQuantity() > 0) {
            $ProcessorID = $sessionInterface->get('cart', []);

            $ProcessorID[] = $id;

            $sessionInterface->set('cart', $ProcessorID);

            return $this->redirectToRoute('shop');
        } else {
            $this->addFlash('error', 'Процесор відсутній на складі');
            return $this->redirectToRoute('shop');
        }
    }

    //Додавання у кошик з кошика
    #[Route('/cart_add_to_cart/{id}', name: 'cart_add_to_cart')]
    public function CartAddToCart(SessionInterface $sessionInterface, EntityManagerInterface $entityManager, $id): Response
    {
        $processor = $entityManager->getRepository(Processor::class)->find($id);

        if ($processor->getStockQuantity() > 0) {
            $ProcessorID = $sessionInterface->get('cart', []);

            $ProcessorID[] = $id;

            $sessionInterface->set('cart', $ProcessorID);

            return $this->redirectToRoute('view_cart');
        } else {
            $this->addFlash('error', 'Процесор відсутній на складі');
            return $this->redirectToRoute('view_cart');
        }
    }

    //Прибирання з кошика
    #[Route('/remove-from-cart/{id}', name: 'remove_from_cart')]
    public function removeFromCart(SessionInterface $sessionInterface, $id): Response
    {
        $ProcessorID = $sessionInterface->get('cart', []);

        $key = array_search($id, $ProcessorID);
        if ($key !== false) {
            unset($ProcessorID[$key]);

            $ProcessorID = array_values($ProcessorID);

            $sessionInterface->set('cart', $ProcessorID);
        }

        return $this->redirectToRoute('view_cart');
    }

    //Очищення кошика
    #[Route('/clear-cart', name: 'clear_cart')]
    public function clearCart(SessionInterface $sessionInterface): Response
    {
        $sessionInterface->remove('cart');

        return $this->redirectToRoute('view_cart');
    }

    #[Route('/order/create', name: 'create_order')]
    public function createOrder(
        CustomersRepository $customersRepository,
        Request             $request,
        ManagerRegistry     $doctrine,
        ProcessorRepository $processorRepository,
        SessionInterface    $sessionInterface
    ): Response
    {
        $entityManager = $doctrine->getManager();
        $ProcessorID = $sessionInterface->get('cart', []);

        $processors = $processorRepository->findBy(['id' => $ProcessorID]);

        $productQuantities = array_count_values($ProcessorID);

        $cartData = [];
        $totalAmount = 0;
        $order = new Orders();

        foreach ($processors as $processor) {
            $orderDetail = new OrderDetails();
            $orderDetail->setQuantity($productQuantities[$processor->getId()]);
            $orderDetail->setProcessor($processor);
            $orderDetail->setOrder($order);
            $entityManager->persist($orderDetail);

            $processorStatistics = $entityManager->getRepository(ProcessorStatistics::class)->findOneBy(['processor' => $processor]);

            $orderCount = $processorStatistics->getOrderCount() + $orderDetail->getQuantity();
            $processorStatistics->setOrderCount($orderCount);

            $entityManager->persist($processorStatistics);

            $cartData[] = [
                'id' => $processor->getId(),
                'image' => $processor->getImage(),
                'name' => $processor->getName(),
                'price' => $processor->getPrice(),
                'quantity' => $productQuantities[$processor->getId()],
            ];

            $totalAmount += $orderDetail->getQuantity() * $processor->getPrice();
        }

        $form = $this->createForm(OrderFormType::class, $order);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $order->setTotalAmount($totalAmount);
            $currentDateTime = new \DateTime();
            $order->setOrderDate($currentDateTime);
            $customer = $customersRepository->find($this->getUser()->getId());
            $order->setCustomer($customer);
            $entityManager->persist($order);
            $entityManager->flush();

            $sessionInterface->set('cart', []);

            return $this->redirectToRoute('shop');
        }

        return $this->render('cart/create.html.twig', [
            'totalAmount' => $totalAmount,
            'cartData' => $cartData,
            'form' => $form->createView(),
            'productQuantities' => $productQuantities,
        ]);
    }
}

