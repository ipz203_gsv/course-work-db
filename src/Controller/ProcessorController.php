<?php

namespace App\Controller;

use App\Entity\Processor;
use App\Entity\ProcessorStatistics;
use App\Entity\Reviews;
use App\Form\EditReviewsFormType;
use App\Form\ReviewsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProcessorController extends AbstractController
{
    #[Route("/processor/{id}", name: "processor_details")]
    public function processorDetails(Processor $processor, EntityManagerInterface $entityManager, Request $request, $id): Response
    {
        $processorStatistics = $entityManager->getRepository(ProcessorStatistics::class)->findOneBy(['processor' => $processor]);
        $reviews = $entityManager->getRepository(Reviews::class)->findBy(['processor' => $processor]);
        $review = $entityManager->getRepository(Reviews::class)->findOneBy(['processor' => $processor, 'customer' => $this->getUser()]);
        $isReviewExist = true;
        if (!$review) {
            $review = new Reviews();
            $isReviewExist = false;
        }
        $form = $this->createForm(ReviewsType::class, $review);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $review->setProcessor($processor);
            $review->setCustomer($this->getUser());
            $review->setCreatedAt(new \DateTime());
            $entityManager->persist($review);
            $entityManager->flush();
            return $this->redirectToRoute('processor_details', ['id' => $id]);
        }
        if (!$processorStatistics) {
            $processorStatistics = new ProcessorStatistics();
            $processorStatistics->setProcessorId($processor);
        }
        $viewCount = $processorStatistics->getViewCount() + 1;
        $processorStatistics->setViewCount($viewCount);

        $entityManager->persist($processorStatistics);
        $entityManager->flush();

        return $this->render('product_page/index.html.twig', [
            'processor' => $processor,
            'form' => $form->createView(),
            'reviews' => $reviews,
            'isReviewExist' => $isReviewExist,
        ]);
    }
}
