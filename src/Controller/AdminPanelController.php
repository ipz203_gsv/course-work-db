<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Entity\Customers;
use App\Entity\OrderDetails;
use App\Entity\Orders;
use App\Entity\Processor;
use App\Entity\ProcessorBrands;
use App\Entity\ProcessorIntegratedGraphics;
use App\Entity\ProcessorSocketTypes;
use App\Entity\ProcessorStatistics;
use App\Entity\Reviews;
use App\Form\CreateBrandFormType;
use App\Form\CreateIntegratedGraphicsFormType;
use App\Form\CreateCategoriesFormType;
use App\Form\CreateProcessorFormType;
use App\Form\CreateReviewsFormType;
use App\Form\CreateSocketFormType;
use App\Form\EditBrandFormType;
use App\Form\EditCategoriesFormType;
use App\Form\EditCustomersFormType;
use App\Form\EditIntegratedGraphicsFormType;
use App\Form\EditOrdersFormType;
use App\Form\EditProcessorFormType;
use App\Form\EditReviewsFormType;
use App\Form\EditSocketFormTypeesType;
use App\Repository\CategoriesRepository;
use App\Repository\CustomersRepository;
use App\Repository\OrderDetailsRepository;
use App\Repository\OrdersRepository;
use App\Repository\ProcessorBrandsRepository;
use App\Repository\ProcessorIntegratedGraphicsRepository;
use App\Repository\ProcessorRepository;
use App\Repository\ProcessorSocketTypesRepository;
use App\Repository\ProcessorStatisticsRepository;
use App\Repository\ReviewsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Config\TwigExtra\InkyConfig;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;


class AdminPanelController extends AbstractController
{
    //Статистика
    #[Route('/admin/panel', name: 'app_admin_panel')]
    public function index(ManagerRegistry                       $doctrine,
                          ProcessorStatisticsRepository         $statisticsRepository,
                          ProcessorRepository                   $processorRepository,
                          ProcessorBrandsRepository             $processorBrandsRepository,
                          ProcessorSocketTypesRepository        $processorSocketTypesRepository,
                          CategoriesRepository                  $categoriesRepository,
                          ProcessorIntegratedGraphicsRepository $integratedGraphicsRepository,
                          ProcessorStatisticsRepository         $processorStatisticsRepository,
                          Request                               $request,
    ): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');
        $brandFilter = $request->query->all()['brandFilter'] ?? null;
        $brands = $processorBrandsRepository->findAll();
        $graphicsFilter = $request->query->all()['graphicsFilter'] ?? null;
        $integratedGraphics = $integratedGraphicsRepository->findAll();
        $coresFilter = $request->query->all()['coresFilter'] ?? null;
        $allProcessors = $processorRepository->findAll();
        $allStatistics = $processorStatisticsRepository->findAll();

        foreach ($allProcessors as $processor) {
            $processorId = $processor->getId();
            $existingStatistic = array_filter($allStatistics, function ($statistic) use ($processorId) {
                return $statistic->getProcessorId()->getId() === $processorId;
            });

            if (empty($existingStatistic)) {
                $newStatistic = new ProcessorStatistics();
                $newStatistic->setProcessorId($processor);
                $entityManager->persist($newStatistic);
            }
        }
        $entityManager->flush();

        $cores = array_unique(array_map(function ($processor) {
                return $processor->getCores();
            }, $allProcessors)
        );
        sort($cores);
        $socketFilter = $request->query->all()['socketFilter'] ?? null;
        $sockets = $processorSocketTypesRepository->findAll();
        $categoryFilter = $request->query->all()['categoryFilter'] ?? null;
        $categories = $categoriesRepository->findAll();

        $cacheMemoryFilter = $request->query->all()['cacheMemoryFilter'] ?? null;
        $cache = array_unique(array_map(function ($processor) {
                return $processor->getCacheMemory();
            }, $allProcessors)
        );
        sort($cache);

        $techProcessFilter = $request->query->all()['techProcessFilter'] ?? null;
        $techProcess = array_unique(array_map(function ($processor) {
                return $processor->getTechProcess();
            }, $allProcessors)
        );
        sort($techProcess);

        $tdpFilter = $request->query->all()['tdpFilter'] ?? null;
        $tdp = array_unique(array_map(function ($processor) {
                return $processor->getTdp();
            }, $allProcessors)
        );
        sort($tdp);

        $minPrice = $request->query->get('minPrice', null);
        $maxPrice = $request->query->get('maxPrice', null);

        $statistics = $entityManager->getRepository(ProcessorStatistics::class)->filterStatistic(
            $field,
            $direction,
            $searchQuery,
            $brandFilter,
            $graphicsFilter,
            $coresFilter,
            $socketFilter,
            $categoryFilter,
            $cacheMemoryFilter,
            $techProcessFilter,
            $tdpFilter,
            $minPrice,
            $maxPrice
        );

        $labels = [];
        $viewCounts = [];
        $orderCounts = [];

        foreach ($statistics as $statistic) {
            $labels[] = $statistic->getProcessorId()->getName(); // Наприклад, використовуйте ім'я процесора як мітку
            $viewCounts[] = $statistic->getViewCount();
            $orderCounts[] = $statistic->getOrderCount();
        }

        return $this->render('admin_panel/index.html.twig', [
            'controller_name' => 'AdminPanelController',
            'labels' => json_encode($labels),
            'viewCounts' => json_encode($viewCounts),
            'orderCounts' => json_encode($orderCounts),
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
            'brandFilter' => $brandFilter,
            'brands' => $brands,
            'graphicsFilter' => $graphicsFilter,
            'integratedGraphics' => $integratedGraphics,
            'coresFilter' => $coresFilter,
            'cores' => $cores,
            'socketFilter' => $socketFilter,
            'sockets' => $sockets,
            'categoryFilter' => $categoryFilter,
            'categories' => $categories,
            'cacheMemoryFilter' => $cacheMemoryFilter,
            'cache' => $cache,
            'techProcessFilter' => $techProcessFilter,
            'techProcess' => $techProcess,
            'tdpFilter' => $tdpFilter,
            'tdp' => $tdp,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
        ]);
    }


    //Процесори
    #[Route('/admin/panel/processors', name: 'admin_panel_processors')]
    public function adminProcessors(
        ManagerRegistry                       $doctrine,
        Request                               $request,
        ProcessorRepository                   $processorRepository, ProcessorBrandsRepository $processorBrandsRepository,
        ProcessorSocketTypesRepository        $processorSocketTypesRepository,
        CategoriesRepository                  $categoriesRepository,
        ProcessorIntegratedGraphicsRepository $integratedGraphicsRepository,
        ProcessorStatisticsRepository         $processorStatisticsRepository
    ): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');
        $brandFilter = $request->query->all()['brandFilter'] ?? null;
        $brands = $processorBrandsRepository->findAll();
        $graphicsFilter = $request->query->all()['graphicsFilter'] ?? null;
        $integratedGraphics = $integratedGraphicsRepository->findAll();
        $coresFilter = $request->query->all()['coresFilter'] ?? null;
        $allProcessors = $processorRepository->findAll();
        $allStatistics = $processorStatisticsRepository->findAll();

        foreach ($allProcessors as $processor) {
            $processorId = $processor->getId();
            $existingStatistic = array_filter($allStatistics, function ($statistic) use ($processorId) {
                return $statistic->getProcessorId()->getId() === $processorId;
            });

            if (empty($existingStatistic)) {
                $newStatistic = new ProcessorStatistics();
                $newStatistic->setProcessorId($processor);
                $entityManager->persist($newStatistic);
            }
        }
        $entityManager->flush();

        $cores = array_unique(array_map(function ($processor) {
                return $processor->getCores();
            }, $allProcessors)
        );
        sort($cores);
        $socketFilter = $request->query->all()['socketFilter'] ?? null;
        $sockets = $processorSocketTypesRepository->findAll();
        $categoryFilter = $request->query->all()['categoryFilter'] ?? null;
        $categories = $categoriesRepository->findAll();

        $cacheMemoryFilter = $request->query->all()['cacheMemoryFilter'] ?? null;
        $cache = array_unique(array_map(function ($processor) {
                return $processor->getCacheMemory();
            }, $allProcessors)
        );
        sort($cache);

        $techProcessFilter = $request->query->all()['techProcessFilter'] ?? null;
        $techProcess = array_unique(array_map(function ($processor) {
                return $processor->getTechProcess();
            }, $allProcessors)
        );
        sort($techProcess);

        $tdpFilter = $request->query->all()['tdpFilter'] ?? null;
        $tdp = array_unique(array_map(function ($processor) {
                return $processor->getTdp();
            }, $allProcessors)
        );
        sort($tdp);

        $minPrice = $request->query->get('minPrice', null);
        $maxPrice = $request->query->get('maxPrice', null);

        $processors = $entityManager->getRepository(Processor::class)->findAllProcessor(
            $field,
            $direction,
            $searchQuery,
            $brandFilter,
            $graphicsFilter,
            $coresFilter,
            $socketFilter,
            $categoryFilter,
            $cacheMemoryFilter,
            $techProcessFilter,
            $tdpFilter,
            $minPrice,
            $maxPrice
        );

        return $this->render('admin_panel/processors/index.html.twig', [
            'processors' => $processors,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
            'brandFilter' => $brandFilter,
            'brands' => $brands,
            'graphicsFilter' => $graphicsFilter,
            'integratedGraphics' => $integratedGraphics,
            'coresFilter' => $coresFilter,
            'cores' => $cores,
            'socketFilter' => $socketFilter,
            'sockets' => $sockets,
            'categoryFilter' => $categoryFilter,
            'categories' => $categories,
            'cacheMemoryFilter' => $cacheMemoryFilter,
            'cache' => $cache,
            'techProcessFilter' => $techProcessFilter,
            'techProcess' => $techProcess,
            'tdpFilter' => $tdpFilter,
            'tdp' => $tdp,
            'minPrice' => $minPrice,
            'maxPrice' => $maxPrice,
        ]);
    }

    #[Route('/admin/panel/processors/create', name: 'create_processors')]
    public function createProcessor(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $processor = new Processor();
        $statistic = new ProcessorStatistics();

        $form = $this->createForm(CreateProcessorFormType::class, $processor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedFile = $form->get('image')->getData();

            if ($uploadedFile) {
                $newFilename = uniqid() . '.' . $uploadedFile->guessExtension();
                $uploadedFile->move('images', $newFilename);
                $processor->setImage($newFilename);
            }
            $statistic->setProcessorId($processor);
            $entityManager->persist($statistic);
            $entityManager->persist($processor);
            $entityManager->flush();

            return new RedirectResponse('/admin/panel/processors');
        }
        return $this->render('admin_panel/processors/create.html.twig', [
            'processor' => $processor,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/processors/edit/{id}', name: 'edit_processors')]
    public function editProcessor(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $processor = $entityManager->getRepository(Processor::class)->find($id);
        $form = $this->createForm(EditProcessorFormType::class, $processor);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedFile = $form->get('image')->getData();

            if ($uploadedFile) {
                $newFilename = uniqid() . '.' . $uploadedFile->guessExtension();
                $uploadedFile->move('images', $newFilename);
                $processor->setImage($newFilename);
            }

            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_processors');
        }

        return $this->render('admin_panel/processors/edit.html.twig', [
            'processor' => $processor,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/processor/delete/{id}', name: 'delete_processor')]
    public function deleteProcessor(ManagerRegistry $doctrine, int $id,): Response
    {
        $entityManager = $doctrine->getManager();
        $processor = $entityManager->getRepository(Processor::class)->find($id);
        $orderDetails = $entityManager->getRepository(OrderDetails::class)->searchByProcessorID($id);
        $reviews = $entityManager->getRepository(Reviews::class)->searchByProcessorID($id);
        $statistics = $entityManager->getRepository(ProcessorStatistics::class)->findBy(['processor' => $processor]);
        foreach ($statistics as $statistic) {
            $entityManager->remove($statistic);
        }

        if (empty($orderDetails) && empty($reviews)) {
            $entityManager->remove($processor);
            $entityManager->flush();
        } else {
            $this->addFlash('danger', 'Процесор не видалено.');
        }

        $this->addFlash('success', 'Процесор видалено успішно.');

        return $this->redirectToRoute('admin_panel_processors');
    }


    //Клієнти
    #[Route('/admin/panel/customers', name: 'admin_panel_customers')]
    public function adminCustomers(ManagerRegistry $doctrine, Request $request, CustomersRepository $customersRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');

        $rolesFilter = $request->query->all()['rolesFilter'] ?? null;
        $allCustomer = $customersRepository->findAll();

        $allRoles = [];
        foreach ($allCustomer as $customer) {
            $customerRoles = $customer->getRoles();
            $allRoles = array_merge($allRoles, $customerRoles);
        }
        $roles = array_unique($allRoles);

        $customers = $entityManager->getRepository(Customers::class)->findAllCustomers($field, $direction, $searchQuery);

        if (!empty($rolesFilter)) {
            $filteredCustomers = [];
            foreach ($customers as $customer) {
                $customerRoles = $customer->getRoles();
                $hasAllRoles = true;

                foreach ($rolesFilter as $role) {
                    if (!in_array($role, $customerRoles)) {
                        $hasAllRoles = false;
                        break;
                    }
                }

                if ($hasAllRoles) {
                    $filteredCustomers[] = $customer;
                }
            }
        } else {
            $filteredCustomers = $customers;
        }


        return $this->render('admin_panel/customers/index.html.twig', [
            'customers' => $filteredCustomers,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
            'rolesFilter' => $rolesFilter,
            'roles' => $roles,
        ]);
    }

    #[Route('/admin/panel/customers/edit/{id}', name: 'edit_customers')]
    public function editCustomers(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $customer = $entityManager->getRepository(Customers::class)->find($id);
        $form = $this->createForm(EditCustomersFormType::class, $customer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_customers');
        }

        return $this->render('admin_panel/customers/edit.html.twig', [
            'customer' => $customer,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/customers/delete/{id}', name: 'delete_customer')]
    public function deleteCustomer(ManagerRegistry $doctrine, Customers $customer, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $customer = $entityManager->getRepository(Customers::class)->find($id);
        $orders = $entityManager->getRepository(Orders::class)->searchByCustomersID($id);
        $reviews = $entityManager->getRepository(Reviews::class)->searchByCustomersID($id);

        if (empty($orders) && empty($reviews)) {
            $entityManager->remove($customer);
            $entityManager->flush();
            $this->addFlash('success', 'Клієнта видалено успішно.');
        } else {
            $this->addFlash('danger', 'Клієнта не видалено.');
        }

        return $this->redirectToRoute('admin_panel_customers');
    }

    //Замовлення
    #[Route('/admin/panel/orders', name: 'admin_panel_orders')]
    public function adminOrders(ManagerRegistry $doctrine, Request $request, OrdersRepository $ordersRepository, OrderDetailsRepository $ordersDetailsRepository): Response
    {
        $entityManager = $doctrine->getManager();

        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');
        $hasComment = $request->query->get('hasComment');

        $orders = $entityManager->getRepository(Orders::class)->findAllOrdered($field, $direction, $searchQuery, $hasComment);
        $ordersDetails = $ordersDetailsRepository->findAll();

        return $this->render('admin_panel/orders/index.html.twig', [
            'orders' => $orders,
            'ordersDetails' => $ordersDetails,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
            'hasComment' => $hasComment,

        ]);
    }

    #[Route('/admin/panel/orders/create', name: 'create_orders')]
    public function createOrder(ManagerRegistry $doctrine, Request $request, CustomersRepository $customersRepository, ProcessorRepository $processorRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $order = new Orders();
        $customers = $customersRepository->findAll();
        $processors = $processorRepository->findAll();
        $details = [];

        if ($request->isMethod('POST')) {
            $address = $request->request->get('address');
            $customerId = $request->request->get('customer'); // Змінна customer містить ID
            $comment = $request->request->get('comment');
            $totalAmount = 0;
            $processors = $request->request->all()['processors'] ?? null;
            $quantities = $request->request->all()['quantities'] ?? null;

            $result = [];

            for ($i = 0; $i < count($processors); $i++) {
                $result[] = [$processors[$i], $quantities[$i]];
            }

            foreach ($result as $processorID) {
                $processor = $processorRepository->find($processorID[0]);
                $orderDetail = new OrderDetails();
                $orderDetail->setQuantity($processorID[1]);
                $orderDetail->setProcessor($processor);
                $orderDetail->setOrder($order);
                $entityManager->persist($orderDetail);
                $totalAmount += $processorID[1] * $processor->getPrice();

            }

            $customer = $customersRepository->find($customerId);

            $order->setAddress($address);
            $order->setCustomer($customer);
            $order->setOrderDate(new \DateTime());
            $order->setComment($comment);
            $order->setTotalAmount($totalAmount);

            $entityManager->persist($order);
            $entityManager->flush();

            return $this->redirectToRoute('admin_panel_orders');
        }
        $selectData = [];
        foreach ($processors as $processor) {
            $selectData[] = [$processor->getName(), $processor->getId()];
        }
        return $this->render('admin_panel/orders/create.html.twig', [
            'details' => $details,
            'customers' => $customers,
            'processors' => $processors,
            'selectData' => $selectData,
        ]);
    }


    #[Route('/admin/panel/orders/edit/{id}', name: 'edit_order')]
    public function editOrder(OrderDetailsRepository $orderDetailsRepository, int $id, ManagerRegistry $doctrine, Request $request, OrdersRepository $ordersRepository, CustomersRepository $customersRepository, ProcessorRepository $processorRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $order = $ordersRepository->find($id);
        $orderDetails = $orderDetailsRepository->findByOrderId($id);

        $customers = $customersRepository->findAll();
        $processors = $processorRepository->findAll();

        if ($request->isMethod('POST')) {
            $address = $request->request->get('address');
            $customerId = $request->request->get('customer');
            $comment = $request->request->get('comment');
            $totalAmount = 0;

            foreach ($orderDetails as $orderDetail) {
                $entityManager->remove($orderDetail);
            }

            $processors = $request->request->all()['processors'] ?? null;
            $quantities = $request->request->all()['quantities'] ?? null;

            $result = [];

            for ($i = 0; $i < count($processors); $i++) {
                $result[] = [$processors[$i], $quantities[$i]];
            }

            foreach ($result as $processorID) {
                $processor = $processorRepository->find($processorID[0]);
                $orderDetail = new OrderDetails();
                $orderDetail->setQuantity($processorID[1]);
                $orderDetail->setProcessor($processor);
                $orderDetail->setOrder($order);
                $entityManager->persist($orderDetail);
                $totalAmount += $processorID[1] * $processor->getPrice();

            }

            $customer = $customersRepository->find($customerId);

            $order->setAddress($address);
            $order->setCustomer($customer);
            $order->setComment($comment);
            $order->setTotalAmount($totalAmount);

            $entityManager->flush();

            return $this->redirectToRoute('admin_panel_orders');
        }
        $selectData = [];
        foreach ($processors as $processor) {
            $selectData[] = [$processor->getName(), $processor->getId()];
        }
        return $this->render('admin_panel/orders/edit.html.twig', [
            'order' => $order,
            'customers' => $customers,
            'processors' => $processors,
            'orderDetails' => $orderDetails,
            'selectData' => $selectData,
        ]);
    }


    #[Route('/admin/panel/orders/delete/{id}', name: 'delete_order')]
    public function deleteOrder(ManagerRegistry $doctrine, Orders $order, int $id,): Response
    {
        $entityManager = $doctrine->getManager();
        $order = $entityManager->getRepository(Orders::class)->find($id);
        $orderDetails = $entityManager->getRepository(OrderDetails::class)->searchByOrderID($id);

        foreach ($orderDetails as $orderDetail) {
            $entityManager->remove($orderDetail);
        }
        $entityManager->remove($order);
        $entityManager->flush();
        $this->addFlash('success', 'Замовлення видалено успішно.');
        return $this->redirectToRoute('admin_panel_orders');
    }


    //Бренди
    #[Route('/admin/panel/brands', name: 'admin_panel_brands')]
    public function adminBrands(ManagerRegistry $doctrine, Request $request, ProcessorBrandsRepository $brandsRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');
        $allBrands = $brandsRepository->findAll();

        $countryFilter = $request->query->all()['countryFilter'] ?? null;
        $country = array_unique(array_map(function ($brand) {
                return $brand->getCountry();
            }, $allBrands)
        );
        sort($country);

        $brands = $entityManager->getRepository(ProcessorBrands::class)->findAllBrands($field, $direction, $searchQuery, $countryFilter);

        return $this->render('admin_panel/brands/index.html.twig', [
            'brands' => $brands,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
            'countryFilter' => $countryFilter,
            'country' => $country,
        ]);
    }

    #[Route('/admin/panel/brands/create', name: 'create_brand')]
    public function createBrand(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $brand = new ProcessorBrands();
        $form = $this->createForm(CreateBrandFormType::class, $brand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($brand);
            $entityManager->flush();

            return new RedirectResponse('/admin/panel/brands');
        }
        return $this->render('admin_panel/brands/create.html.twig', [
            'brand' => $brand,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/brands/edit/{id}', name: 'edit_brand')]
    public function editBrand(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $brand = $entityManager->getRepository(ProcessorBrands::class)->find($id);
        $form = $this->createForm(EditBrandFormType::class, $brand);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_brands');
        }

        return $this->render('admin_panel/brands/edit.html.twig', [
            'brand' => $brand,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/brands/delete/{id}', name: 'delete_brand')]
    public function deleteBrand(ManagerRegistry $doctrine, ProcessorBrands $brand, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $brand = $entityManager->getRepository(ProcessorBrands::class)->find($id);
        $processor = $entityManager->getRepository(Processor::class)->searchByBrandID($id);

        if (empty($processor)) {
            $entityManager->remove($brand);
            $entityManager->flush();
            $this->addFlash('success', 'Бренд видалено успішно.');
        } else {
            $this->addFlash('danger', 'Бренд не видалено.');
        }
        return $this->redirectToRoute('admin_panel_brands');
    }


    //Інтегрована графіка
    #[Route('/admin/panel/integratedgraphics', name: 'admin_panel_integratedgraphics')]
    public function integratedGraphics(ManagerRegistry $doctrine, Request $request, ProcessorIntegratedGraphicsRepository $integratedGraphicsRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');

        $integratedGraphics = $entityManager->getRepository(ProcessorIntegratedGraphics::class)->findAllGraphics($field, $direction, $searchQuery);

        return $this->render('admin_panel/integratedgraphics/index.html.twig', [
            'integratedGraphics' => $integratedGraphics,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
        ]);
    }

    #[Route('/admin/panel/integratedgraphics/create', name: 'create_integratedgraphics')]
    public function createIntegratedGraphics(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $graphics = new ProcessorIntegratedGraphics();
        $form = $this->createForm(CreateIntegratedGraphicsFormType::class, $graphics);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($graphics);
            $entityManager->flush();

            return new RedirectResponse('/admin/panel/integratedgraphics');
        }
        return $this->render('admin_panel/integratedgraphics/create.html.twig', [
            'graphics' => $graphics,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/integratedgraphics/edit/{id}', name: 'edit_integratedgraphics')]
    public function editIntegratedGraphics(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $graphics = $entityManager->getRepository(ProcessorIntegratedGraphics::class)->find($id);
        $form = $this->createForm(EditIntegratedGraphicsFormType::class, $graphics);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_integratedgraphics');
        }

        return $this->render('admin_panel/integratedgraphics/edit.html.twig', [
            'graphics' => $graphics,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/integratedgraphics/delete/{id}', name: 'delete_integratedgraphics')]
    public function deleteIntegratedGraphics(ManagerRegistry $doctrine, ProcessorIntegratedGraphics $integratedgraphics, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $integratedgraphics = $entityManager->getRepository(ProcessorIntegratedGraphics::class)->find($id);
        $processor = $entityManager->getRepository(Processor::class)->searchByIntegratedGraphicsID($id);

        if (empty($processor)) {
            $entityManager->remove($integratedgraphics);
            $entityManager->flush();
            $this->addFlash('success', 'Інтегровану графіку видалено успішно.');
        } else {
            $this->addFlash('danger', 'Інтегровану графіку не видалено.');
        }
        return $this->redirectToRoute('admin_panel_integratedgraphics');
    }


    //Тип роз'єму
    #[Route('/admin/panel/sockettypes', name: 'admin_panel_sockettypes')]
    public function socketTypes(ManagerRegistry $doctrine, Request $request, ProcessorSocketTypesRepository $socketTypesRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');

        $socketTypes = $entityManager->getRepository(ProcessorSocketTypes::class)->findAllSocket($field, $direction, $searchQuery);

        return $this->render('admin_panel/sockettypes/index.html.twig', [
            'socketTypes' => $socketTypes,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
        ]);
    }


    #[Route('/admin/panel/sockettypes/create', name: 'create_sockettypes')]
    public function createSockettypes(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $socket = new ProcessorSocketTypes();
        $form = $this->createForm(CreateSocketFormType::class, $socket);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($socket);
            $entityManager->flush();

            return new RedirectResponse('/admin/panel/sockettypes');
        }
        return $this->render('admin_panel/sockettypes/create.html.twig', [
            'socket' => $socket,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/sockettypes/edit/{id}', name: 'edit_sockettypes')]
    public function editSockettypes(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $socket = $entityManager->getRepository(ProcessorSocketTypes::class)->find($id);
        $form = $this->createForm(EditSocketFormTypeesType::class, $socket);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_sockettypes');
        }

        return $this->render('admin_panel/sockettypes/edit.html.twig', [
            'socket' => $socket,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/sockettypes/delete/{id}', name: 'delete_sockettypes')]
    public function deleteSocketTypes(ManagerRegistry $doctrine, ProcessorSocketTypes $socketType, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $socketType = $entityManager->getRepository(ProcessorSocketTypes::class)->find($id);
        $processor = $entityManager->getRepository(Processor::class)->searchBySocketTypesID($id);

        if (empty($processor)) {
            $entityManager->remove($socketType);
            $entityManager->flush();
            $this->addFlash('success', 'Тип роз\'єму видалено успішно.');
        } else {
            $this->addFlash('danger', 'Тип роз\'єму не видалено.');
        }
        return $this->redirectToRoute('admin_panel_sockettypes');
    }


    //Відгук про товар
    #[Route('/admin/panel/reviews', name: 'admin_panel_reviews')]
    public function Reviews(ManagerRegistry $doctrine, Request $request, ReviewsRepository $reviewsRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');
        $allReviews = $reviewsRepository->findAll();

        $ratingFilter = $request->query->all()['ratingFilter'] ?? null;
        $rating = array_unique(array_map(function ($review) {
                return $review->getRating();
            }, $allReviews)
        );
        sort($rating);

        $reviews = $entityManager->getRepository(Reviews::class)->findAllReviews($field, $direction, $searchQuery, $ratingFilter);
        return $this->render('admin_panel/reviews/index.html.twig', [
            'reviews' => $reviews,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
            'ratingFilter' => $ratingFilter,
            'rating' => $rating,

        ]);
    }


    #[Route('/admin/panel/reviews/create', name: 'create_reviews')]
    public function createReviews(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $review = new Reviews();
        $form = $this->createForm(CreateReviewsFormType::class, $review);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $review->setCreatedAt(new \DateTime());
            $entityManager->persist($review);
            $entityManager->flush();

            return new RedirectResponse('/admin/panel/reviews');
        }
        return $this->render('admin_panel/reviews/create.html.twig', [
            'review' => $review,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/reviews/edit/{id}', name: 'edit_reviews')]
    public function editReviews(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $reviews = $entityManager->getRepository(Reviews::class)->find($id);
        $form = $this->createForm(EditReviewsFormType::class, $reviews);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_reviews');
        }

        return $this->render('admin_panel/reviews/edit.html.twig', [
            'reviews' => $reviews,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/reviews/delete/{id}', name: 'delete_reviews')]
    public function deleteReviews(ManagerRegistry $doctrine, Reviews $reviews, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $reviews = $entityManager->getRepository(Reviews::class)->find($id);

        $entityManager->remove($reviews);
        $entityManager->flush();
        $this->addFlash('success', 'Відгук видалено успішно.');

        return $this->redirectToRoute('admin_panel_reviews');
    }


    //Категорії
    #[Route('/admin/panel/categories', name: 'admin_panel_categories')]
    public function Categories(ManagerRegistry $doctrine, Request $request, CategoriesRepository $categoriesRepository): Response
    {
        $entityManager = $doctrine->getManager();
        $sortBy = $request->query->get('sortOption', 'date_desc');
        list($field, $direction) = explode("_", $sortBy);
        $searchQuery = $request->query->get('searchQuery');

        $categories = $entityManager->getRepository(Categories::class)->findAllCategories($field, $direction, $searchQuery);
        return $this->render('admin_panel/categories/index.html.twig', [
            'categories' => $categories,
            "sortBy" => $sortBy,
            'searchQuery' => $searchQuery,
        ]);
    }

    #[Route('/admin/panel/categories/create', name: 'create_categories')]
    public function createCategories(ManagerRegistry $doctrine, Request $request): Response
    {
        $entityManager = $doctrine->getManager();
        $categories = new Categories();
        $form = $this->createForm(CreateCategoriesFormType::class, $categories);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($categories);
            $entityManager->flush();

            return new RedirectResponse('/admin/panel/categories');
        }
        return $this->render('admin_panel/categories/create.html.twig', [
            'categories' => $categories,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/categories/edit/{id}', name: 'edit_categories')]
    public function editCategories(Request $request, $id, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $categories = $entityManager->getRepository(Categories::class)->find($id);
        $form = $this->createForm(EditCategoriesFormType::class, $categories);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            return $this->redirectToRoute('admin_panel_categories');
        }

        return $this->render('admin_panel/categories/edit.html.twig', [
            'categories' => $categories,
            'form' => $form->createView()
        ]);
    }

    #[Route('/admin/panel/categories/delete/{id}', name: 'delete_categories')]
    public function deleteCategories(ManagerRegistry $doctrine, Categories $categories, int $id): Response
    {
        $entityManager = $doctrine->getManager();
        $categories = $entityManager->getRepository(Categories::class)->find($id);
        $processor = $entityManager->getRepository(Processor::class)->searchByCategoriesID($id);

        if (empty($processor)) {
            $entityManager->remove($categories);
            $entityManager->flush();
            $this->addFlash('success', 'Категорію видалено успішно.');
        } else {
            $this->addFlash('danger', 'Категорію не видалено.');
        }

        return $this->redirectToRoute('admin_panel_categories');
    }
}