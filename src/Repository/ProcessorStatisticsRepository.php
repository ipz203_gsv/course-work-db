<?php

namespace App\Repository;

use App\Entity\ProcessorStatistics;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProcessorStatistics>
 *
 * @method ProcessorStatistics|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessorStatistics|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessorStatistics[]    findAll()
 * @method ProcessorStatistics[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessorStatisticsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessorStatistics::class);
    }

    public function filterStatistic($sortBy, $sortDirection, $searchQuery, $brandFilter, $graphicsFilter, $coresFilter, $socketFilter, $categoryFilter, $cacheMemoryFilter, $techProcessFilter, $tdpFilter, $minPrice, $maxPrice): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('stat');
        $queryBuilder->join('stat.processor', 'p');
        $queryBuilder->join('p.socketType', 's');
        $queryBuilder->join('p.integratedGraphics', 'i');
        $queryBuilder->join('p.category', 'c');
        $queryBuilder->join('p.brand', 'b');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(p.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(s.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(i.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(c.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(b.name)', ':value'),
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');


        if (!empty($brandFilter)) {
            $queryBuilder->andWhere('b.id IN (:brand)')
                ->setParameter('brand', $brandFilter);
        }

        if (!empty($graphicsFilter)) {
            $queryBuilder->andWhere('i.id IN (:integratedGraphics)')
                ->setParameter('integratedGraphics', $graphicsFilter);
        }

        if (!empty($coresFilter)) {
            $queryBuilder->andWhere('p.cores IN (:cores)')
                ->setParameter('cores', $coresFilter);
        }

        if (!empty($socketFilter)) {
            $queryBuilder->andWhere('s.id IN (:socket)')
                ->setParameter('socket', $socketFilter);
        }

        if (!empty($categoryFilter)) {
            $queryBuilder->andWhere('c.id IN (:category)')
                ->setParameter('category', $categoryFilter);
        }

        if (!empty($cacheMemoryFilter)) {
            $queryBuilder->andWhere('p.cacheMemory IN (:cacheMemory)')
                ->setParameter('cacheMemory', $cacheMemoryFilter);
        }

        if (!empty($techProcessFilter)) {
            $queryBuilder->andWhere('p.techProcess IN (:techProcess)')
                ->setParameter('techProcess', $techProcessFilter);
        }

        if (!empty($tdpFilter)) {
            $queryBuilder->andWhere('p.tdp IN (:tdp)')
                ->setParameter('tdp', $tdpFilter);
        }

        if ($minPrice !== null && $maxPrice !== null && $minPrice !== "" && $maxPrice !== "") {
            $queryBuilder->andWhere('p.price BETWEEN :minPrice AND :maxPrice')
                ->setParameter('minPrice', $minPrice)
                ->setParameter('maxPrice', $maxPrice);
        }


        switch ($sortBy) {
            case 'quantity':
                $queryBuilder->orderBy('p.stock_quantity', ($sortDirection));
                break;
            case 'tdp':
                $queryBuilder->orderBy('p.tdp', ($sortDirection));
                break;
            case 'date':
                $queryBuilder->orderBy('p.created', ($sortDirection));
                break;
            case 'cores':
                $queryBuilder->orderBy('p.cores', ($sortDirection));
                break;
            case 'price':
                $queryBuilder->orderBy('p.price', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('p.created', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();

    }
}
