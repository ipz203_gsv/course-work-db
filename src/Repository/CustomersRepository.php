<?php

namespace App\Repository;

use App\Entity\Customers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @extends ServiceEntityRepository<Customers>
 * @implements PasswordUpgraderInterface<Customers>
 *
 * @method Customers|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customers|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customers[]    findAll()
 * @method Customers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomersRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Customers::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof Customers) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $user::class));
        }

        $user->setPassword($newHashedPassword);
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
    }


    public function findAllCustomers($sortBy, $sortDirection, $searchQuery): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('c');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(c.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(c.phone)', ':value'),
                $queryBuilder->expr()->like('LOWER(c.email)', ':value')

            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');

        switch ($sortBy) {
            case 'date':
                $queryBuilder->orderBy('c.id', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('c.id', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
