<?php

namespace App\Repository;

use App\Entity\Categories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Categories>
 *
 * @method Categories|null find($id, $lockMode = null, $lockVersion = null)
 * @method Categories|null findOneBy(array $criteria, array $orderBy = null)
 * @method Categories[]    findAll()
 * @method Categories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Categories::class);
    }

    public function findAllCategories($sortBy, $sortDirection, $searchQuery): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('c');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(c.name)', ':value'),
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');


        switch ($sortBy) {
            case 'name':
                $queryBuilder->orderBy('c.name', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('c.name', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
