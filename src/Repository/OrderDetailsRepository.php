<?php

namespace App\Repository;

use App\Entity\OrderDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<OrderDetails>
 *
 * @method OrderDetails|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderDetails|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderDetails[]    findAll()
 * @method OrderDetails[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderDetails::class);
    }

    public function searchByOrderID($orderID): array
    {
        $orderID = strtolower($orderID);

        $qb = $this->createQueryBuilder('o');
        $qb->join('o.order', 'p');

        $qb->andWhere('p.id = :orderID')
            ->setParameter('orderID', $orderID);

        return $qb->getQuery()->getResult();
    }

    public function searchByProcessorID($processorID): array
    {
        $processorID = strtolower($processorID);

        $qb = $this->createQueryBuilder('o');
        $qb->join('o.processor', 'p');

        $qb->andWhere('p.id = :processorID')
            ->setParameter('processorID', $processorID);

        return $qb->getQuery()->getResult();
    }

    public function findByOrderId($orderId)
    {
        return $this->createQueryBuilder('e')
            ->join('e.order', 'o')
            ->andWhere('o.id = :orderId')
            ->setParameter('orderId', $orderId)
            ->getQuery()
            ->getResult();
    }
}
