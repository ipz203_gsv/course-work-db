<?php

namespace App\Repository;

use App\Entity\Reviews;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Reviews>
 *
 * @method Reviews|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reviews|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reviews[]    findAll()
 * @method Reviews[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reviews::class);
    }

    public function searchByProcessorID($processorID): array
    {
        $processorID = strtolower($processorID);

        $qb = $this->createQueryBuilder('o');
        $qb->join('o.processor', 'p');

        $qb->andWhere('p.id = :processorID')
            ->setParameter('processorID', $processorID);

        return $qb->getQuery()->getResult();
    }

    public function searchByCustomersID($customerID): array
    {
        $customerID = strtolower($customerID);

        $qb = $this->createQueryBuilder('o');
        $qb->join('o.customer', 'p');

        $qb->andWhere('p.id = :customerID')
            ->setParameter('customerID', $customerID);

        return $qb->getQuery()->getResult();
    }


    public function findAllReviews($sortBy, $sortDirection, $searchQuery, $ratingFilter): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('r');
        $queryBuilder->join('r.processor', 'p');
        $queryBuilder->join('r.customer', 'c');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(p.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(c.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(r.rating)', ':value'),
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');

        if (!empty($ratingFilter)) {
            $queryBuilder->andWhere('r.rating IN (:rating)')
                ->setParameter('rating', $ratingFilter);
        }

        switch ($sortBy) {
            case 'date':
                $queryBuilder->orderBy('r.created_at', ($sortDirection));
                break;
            case 'rating':
                $queryBuilder->orderBy('r.rating', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('r.created', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
