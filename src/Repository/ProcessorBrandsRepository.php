<?php

namespace App\Repository;

use App\Entity\ProcessorBrands;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProcessorBrands>
 *
 * @method ProcessorBrands|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessorBrands|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessorBrands[]    findAll()
 * @method ProcessorBrands[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessorBrandsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessorBrands::class);
    }
    public function findAllBrands($sortBy, $sortDirection, $searchQuery, $countryFilter): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('b');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(b.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(b.country)', ':value'),
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');

        if (!empty($countryFilter)) {
            $queryBuilder->andWhere('b.country IN (:country)')
                ->setParameter('country', $countryFilter);
        }

        switch ($sortBy) {
            case 'name':
                $queryBuilder->orderBy('b.name', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('b.name', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
