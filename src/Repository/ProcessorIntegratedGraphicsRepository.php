<?php

namespace App\Repository;

use App\Entity\ProcessorIntegratedGraphics;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProcessorIntegratedGraphics>
 *
 * @method ProcessorIntegratedGraphics|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessorIntegratedGraphics|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessorIntegratedGraphics[]    findAll()
 * @method ProcessorIntegratedGraphics[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessorIntegratedGraphicsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessorIntegratedGraphics::class);
    }

    public function findAllGraphics($sortBy, $sortDirection, $searchQuery): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('g');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(g.name)', ':value'),
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');

        switch ($sortBy) {
            case 'date':
                $queryBuilder->orderBy('g.created', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('g.created', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
