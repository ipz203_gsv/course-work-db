<?php

namespace App\Repository;

use App\Entity\ProcessorSocketTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProcessorSocketTypes>
 *
 * @method ProcessorSocketTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessorSocketTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessorSocketTypes[]    findAll()
 * @method ProcessorSocketTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessorSocketTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcessorSocketTypes::class);
    }

    public function findAllSocket($sortBy, $sortDirection, $searchQuery): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('s');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(s.name)', ':value'),
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');


        switch ($sortBy) {
            case 'date':
                $queryBuilder->orderBy('s.created', ($sortDirection));
                break;
            case 'name':
                $queryBuilder->orderBy('s.name', ($sortDirection));
                break;
            case 'capacity':
                $queryBuilder->orderBy('s.capacity', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('s.name', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
