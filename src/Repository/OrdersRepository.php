<?php

namespace App\Repository;

use App\Entity\Orders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Orders>
 *
 * @method Orders|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orders|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orders[]    findAll()
 * @method Orders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Orders::class);
    }

    public function searchByCustomersID($customerID): array
    {
        $customerID = strtolower($customerID);

        $qb = $this->createQueryBuilder('o');
        $qb->join('o.customer', 'c');

        $qb->andWhere('c.id = :customerID')
            ->setParameter('customerID', $customerID);

        return $qb->getQuery()->getResult();
    }

    public function findAllOrdered($sortBy, $sortDirection, $searchQuery, $hasComment): array
    {
        $searchQuery = strtolower($searchQuery);
        $queryBuilder = $this->createQueryBuilder('o');
        $queryBuilder->join('o.customer', 'c');

        $queryBuilder->where(
            $queryBuilder->expr()->orX(
                $queryBuilder->expr()->like('LOWER(c.name)', ':value'),
                $queryBuilder->expr()->like('LOWER(c.phone)', ':value'),
                $queryBuilder->expr()->like('LOWER(c.email)', ':value')
            )
        )
            ->setParameter('value', '%' . $searchQuery . '%');


        if ($hasComment && $hasComment != "") {
            $queryBuilder->andWhere($queryBuilder->expr()->neq('o.comment', ':emptyComment'))
                ->setParameter('emptyComment', '');
        }


        switch ($sortBy) {
            case 'date':
                $queryBuilder->orderBy('o.orderdate', ($sortDirection));
                break;
            case 'price':
                $queryBuilder->orderBy('o.totalamount', ($sortDirection));
                break;
            default:
                //
                $queryBuilder->orderBy('o.orderdate', ($sortDirection));
                break;
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
