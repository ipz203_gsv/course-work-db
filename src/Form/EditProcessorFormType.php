<?php

namespace App\Form;

use App\Entity\Categories;
use App\Entity\Processor;
use App\Entity\ProcessorBrands;
use App\Entity\ProcessorIntegratedGraphics;
use App\Entity\ProcessorSocketTypes;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditProcessorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'Назва процесору',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => 'Назва процесору',
            ])
            ->add('image', FileType::class, [
                'label' => 'Зображення',
                'required' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'mt-2 p-1 border rounded focus:outline-none focus:shadow-outline',
                ],
            ])


            ->add('cores', null, [
                'attr' => [
                    'placeholder' => 'Кількість ядер',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Кількість ядер',
            ])
            ->add('threads', null, [
                'attr' => [
                    'placeholder' => 'Кількість потоків',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Кількість потоків',
            ])
            ->add('techProcess', null, [
                'attr' => [
                    'placeholder' => 'Техпроцес',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Техпроцес   ',
            ])
            ->add('tdp', null, [
                'attr' => [
                    'placeholder' => 'Потужність TDP',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Потужність TDP',
            ])
            ->add('cacheMemory', null, [
                'attr' => [
                    'placeholder' => 'Обсяг кеш пам\'яті',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Обсяг кеш пам\'яті',
            ])
            ->add('baseClockSpeed', null, [
                'attr' => [
                    'placeholder' => 'Тактова частота (Base)',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Тактова частота (Базова)',
            ])
            ->add('maxClockSpeed', null, [
                'attr' => [
                    'placeholder' => 'Тактова частота (Max)',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Тактова частота (Максимальна)',
            ])
            ->add('brand', EntityType::class, [
                'class' => ProcessorBrands::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => 'Бренд',
            ])
            ->add('socketType', EntityType::class, [
                'class' => ProcessorSocketTypes::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => 'Сокет',
            ])
            ->add('integratedGraphics', EntityType::class, [
                'class' => ProcessorIntegratedGraphics::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => 'Інтегрована графіка',
            ])
            ->add('category', EntityType::class, [
                'class' => Categories::class,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => 'Категорія',
            ])
            ->add('created',null,[
                'attr' => [
                    'class' => 'mt-2 md:w-2/5 bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-6 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => false,
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'choice_label' => 'Опис',
                    'class' => 'mt-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-20 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => 'Опис',
            ])
            ->add('stock_quantity', null, [
                'attr' => [
                    'placeholder' => 'Кількість на складі',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Кількість на складі',
            ])
            ->add('price', null, [
                'attr' => [
                    'placeholder' => 'Ціна',
                    'class' => 'mt-2 mb-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white', // додано ml-4
                ],
                'label' => 'Ціна',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Зберегти',
                'attr' => [
                    'class' => 'mb-2 mt-2 px-2.5 py-1.5 rounded-md text-white bg-indigo-500',
                ],
            ]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Processor::class,
        ]);
    }
}
