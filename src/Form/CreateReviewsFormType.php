<?php

namespace App\Form;

use App\Entity\Customers;
use App\Entity\Processor;
use App\Entity\Reviews;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class CreateReviewsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('rating', IntegerType::class, [
                'constraints' => [
                    new Range([
                        'min' => 1,
                        'max' => 5,
                        'minMessage' => 'The rating must be at least 1',
                        'maxMessage' => 'The rating cannot be greater than 5',
                    ]),
                    new NotBlank(['message' => 'Будь ласка, введіть оцінку']),
                ],
            ])
            ->add('comment')
            ->add('processor', EntityType::class, [
                'class' => Processor::class,
'choice_label' => 'name',
            ])
            ->add('customer', EntityType::class, [
                'class' => Customers::class,
'choice_label' => 'name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reviews::class,
        ]);
    }
}
