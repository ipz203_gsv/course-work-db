<?php

namespace App\Form;

use App\Entity\Customers;
use libphonenumber\PhoneNumber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => [
                    'placeholder' => 'Ім\'я',
                    'class' => 'w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-1xl p-3 border-none block mt-2 mb-4',
                ],
                'label' =>'Ім\'я',

            ])
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'Пошта',
                    'class' => 'w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-1xl p-3 border-none block mt-2 mb-4',
                ],
                'label' => 'Пошта',
            ])
            ->add('phone', null, [
                'label' => 'Номер телефону',
                'attr' => [
                    'placeholder' => 'Номер телефону',
                    'class' => 'w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-1xl p-3 border-none block mt-2 mb-4',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Будь ласка, введіть номер телефону.',
                    ]),
                    new Regex([
                        'pattern' => '/^[0-9]+$/',
                        'message' => 'Номер телефону може містити лише цифри.',
                    ]),
                ],
                ])
            ->add('plainPassword', PasswordType::class, [
                'label' => 'Пароль',
                'mapped' => false,
                'attr' => [
                    'class' => 'w-full shadow-inner bg-gray-100 rounded-lg placeholder-black text-1xl p-3 border-none block mt-2 mb-2',
                    'autocomplete' => 'Пароль',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Будьласка введіть пароль',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Ваш пароль повинен містити щонайменше 6 символів',
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'Я погоджуюсь з усіми умовами',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
                'attr' => [
                    'class' => 'mt-4 mb-4 ml-2 items-center',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Зареєструватись',
                'attr' => [
                    'class' => 'mb-2 mt-2 px-2.5 py-1.5 rounded-md text-white bg-indigo-500',
                ],
            ]);
    }


    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customers::class,
        ]);
    }
}
