<?php

namespace App\Form;

use App\Entity\OrderDetails;
use App\Entity\Orders;
use App\Entity\Processor;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditOrderDetailsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantity')
            ->add('order', EntityType::class, [
                'class' => Orders::class,
'choice_label' => 'id',
            ])
            ->add('processor', EntityType::class, [
                'class' => Processor::class,
'choice_label' => 'name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderDetails::class,
        ]);
    }
}
