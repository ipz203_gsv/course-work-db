<?php

namespace App\Form;

use App\Entity\ProcessorBrands;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateBrandFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name',TextType::class, [
                'attr' => [
                    'placeholder' => 'Назва бренду',
                    'class' => 'mt-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => false,
            ])
            ->add('country',TextType::class, [
                'attr' => [
                    'placeholder' => 'Країна',
                    'class' => 'mt-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => false,
            ])
            ->add('website',TextType::class, [
                'attr' => [
                    'placeholder' => 'Веб сайт',
                    'class' => 'mt-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-10 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Зберегти',
                'attr' => [
                    'class' => 'mb-2 mt-2 px-2.5 py-1.5 rounded-md text-white bg-indigo-500',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProcessorBrands::class,
        ]);
    }
}
