<?php

namespace App\Form;

use App\Entity\Customers;
use App\Entity\Processor;
use App\Entity\Reviews;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ReviewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('rating', ChoiceType::class, [
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                ],
                'label' => 'Оцінка',
                'attr' => [
                    'class' => 'form-control',
                ],
            ])
            ->add('comment', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Коментар до замовлення',
                    'class' => 'mt-2 w-full bg-gray-100 rounded border border-gray-400 leading-normal resize-none h-20 py-2 px-3 font-medium placeholder-gray-600 focus:outline-none focus:bg-white',
                ],
                'label' => false,

            ])
            ->add('submit', SubmitType::class, [
            'label' => 'Коментувати',
            'attr' => [
                'class' => 'mb-2 mt-2 px-2.5 py-1.5 rounded-md text-white bg-indigo-500',
            ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reviews::class,
        ]);
    }
}
