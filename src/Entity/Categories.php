<?php

namespace App\Entity;

use App\Repository\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoriesRepository::class)]
class Categories
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'category_id', targetEntity: Processor::class)]
    private Collection $products;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Processor::class)]
    private Collection $processor;

    #[ORM\OneToMany(mappedBy: 'category', targetEntity: Processor::class)]
    private Collection $processors;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->processor = new ArrayCollection();
        $this->processors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Processor $product): static
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setCategory($this);
        }

        return $this;
    }

    public function removeProduct(Processor $product): static
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getCategory() === $this) {
                $product->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessor(): Collection
    {
        return $this->processor;
    }

    public function addProcessorDetail(Processor $processor): static
    {
        if (!$this->processor->contains($processor)) {
            $this->processor->add($processor);
            $processor->setCategory($this);
        }

        return $this;
    }

    public function removeProcessorDetail(Processor $processor): static
    {
        if ($this->processor->removeElement($processor)) {
            // set the owning side to null (unless already changed)
            if ($processor->getCategory() === $this) {
                $processor->setCategory(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessors(): Collection
    {
        return $this->processors;
    }

    public function addProcessor(Processor $processor): static
    {
        if (!$this->processors->contains($processor)) {
            $this->processors->add($processor);
            $processor->setCategory($this);
        }

        return $this;
    }

    public function removeProcessor(Processor $processor): static
    {
        if ($this->processors->removeElement($processor)) {
            // set the owning side to null (unless already changed)
            if ($processor->getCategory() === $this) {
                $processor->setCategory(null);
            }
        }

        return $this;
    }
}
