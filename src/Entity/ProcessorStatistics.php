<?php

namespace App\Entity;

use App\Repository\ProcessorStatisticsRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProcessorStatisticsRepository::class)]
class ProcessorStatistics
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Processor $processor = null;

    #[ORM\Column]
    private ?int $viewCount = 0;

    #[ORM\Column]
    private ?int $orderCount = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProcessorId(): ?Processor
    {
        return $this->processor;
    }

    public function setProcessorId(Processor $processor): static
    {
        $this->processor = $processor;

        return $this;
    }

    public function getViewCount(): ?int
    {
        return $this->viewCount;
    }

    public function setViewCount(int $viewCount): static
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    public function getOrderCount(): ?int
    {
        return $this->orderCount;
    }

    public function setOrderCount(int $orderCount): static
    {
        $this->orderCount = $orderCount;

        return $this;
    }
}
