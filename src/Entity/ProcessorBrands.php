<?php

namespace App\Entity;

use App\Repository\ProcessorBrandsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProcessorBrandsRepository::class)]
class ProcessorBrands
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $country = null;

    #[ORM\Column(length: 255)]
    private ?string $website = null;

    #[ORM\OneToMany(mappedBy: 'brand_id', targetEntity: Processor::class)]
    private Collection $processor;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Processor::class)]
    private Collection $processors;

    public function __construct()
    {
        $this->processor = new ArrayCollection();
        $this->processors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): static
    {
        $this->country = $country;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): static
    {
        $this->website = $website;

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessor(): Collection
    {
        return $this->processor;
    }

    public function addProcessor(Processor $processor): static
    {
        if (!$this->processor->contains($processor)) {
            $this->processor->add($processor);
            $processor->setBrand($this);
        }

        return $this;
    }

    public function removeProcessor(Processor $processor): static
    {
        if ($this->processor->removeElement($processor)) {
            // set the owning side to null (unless already changed)
            if ($processor->getBrand() === $this) {
                $processor->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessors(): Collection
    {
        return $this->processors;
    }

}
