<?php

namespace App\Entity;

use App\Repository\ProcessorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProcessorRepository::class)]
class Processor
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $image = null;
    #[ORM\ManyToOne(inversedBy: 'processors')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProcessorBrands $brand = null;

    #[ORM\ManyToOne(inversedBy: 'processors')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ProcessorSocketTypes $socketType = null;

    #[ORM\Column]
    private ?int $cores = null;

    #[ORM\Column]
    private ?int $threads = null;

    #[ORM\Column]
    private ?float $baseClockSpeed = null;

    #[ORM\Column]
    private ?float $maxClockSpeed = null;

    #[ORM\Column]
    private ?int $cacheMemory = null;

    #[ORM\Column]
    private ?int $techProcess = null;

    #[ORM\Column]
    private ?int $tdp = null;

    #[ORM\ManyToOne(inversedBy: 'processors')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ProcessorIntegratedGraphics $integratedGraphics = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $created = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 2)]
    private ?string $price = null;

    #[ORM\Column]
    private ?int $stock_quantity = null;

    #[ORM\ManyToOne(inversedBy: 'processors')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Categories $category = null;

    #[ORM\Column]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'processor_id', targetEntity: OrderDetails::class)]
    private Collection $orderDetails;

    public function __construct()
    {
        $this->orderDetails = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function getBrand(): ?ProcessorBrands
    {
        return $this->brand;
    }

    public function setBrand(?ProcessorBrands $brand): static
    {
        $this->brand = $brand;

        return $this;
    }

    public function getSocketType(): ?ProcessorSocketTypes
    {
        return $this->socketType;
    }

    public function setSocketType(?ProcessorSocketTypes $socket_type): static
    {
        $this->socketType = $socket_type;


        return $this;
    }

    public function getCores(): ?int
    {
        return $this->cores;
    }

    public function setCores(int $cores): static
    {
        $this->cores = $cores;

        return $this;
    }

    public function getThreads(): ?int
    {
        return $this->threads;
    }

    public function setThreads(int $threads): static
    {
        $this->threads = $threads;

        return $this;
    }

    public function getBaseClockSpeed(): ?float
    {
        return $this->baseClockSpeed;
    }

    public function setBaseClockSpeed(float $base_clock_speed): static
    {
        $this->baseClockSpeed = $base_clock_speed;

        return $this;
    }

    public function getMaxClockSpeed(): ?float
    {
        return $this->maxClockSpeed;
    }

    public function setMaxClockSpeed(float $max_clock_speed): static
    {
        $this->maxClockSpeed = $max_clock_speed;

        return $this;
    }

    public function getCacheMemory(): ?int
    {
        return $this->cacheMemory;
    }

    public function setCacheMemory(int $cache_memory): static
    {
        $this->cacheMemory = $cache_memory;

        return $this;
    }

    public function getTechProcess(): ?int
    {
        return $this->techProcess;
    }

    public function setTechProcess(int $tech_process): static
    {
        $this->techProcess = $tech_process;

        return $this;
    }

    public function getTdp(): ?int
    {
        return $this->tdp;
    }

    public function setTdp(int $tdp): static
    {
        $this->tdp = $tdp;

        return $this;
    }

    public function getIntegratedGraphics(): ?ProcessorIntegratedGraphics
    {
        return $this->integratedGraphics;
    }

    public function setIntegratedGraphics(?ProcessorIntegratedGraphics $integrated_graphics): static
    {
        $this->integratedGraphics = $integrated_graphics;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): static
    {
        $this->created = $created;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getStockQuantity(): ?int
    {
        return $this->stock_quantity;
    }

    public function setStockQuantity(int $stock_quantity): static
    {
        $this->stock_quantity = $stock_quantity;

        return $this;
    }

    public function getCategory(): ?Categories
    {
        return $this->category;
    }

    public function setCategory(?Categories $category): static
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, OrderDetails>
     */
    public function getOrderDetails(): Collection
    {
        return $this->orderDetails;
    }

    public function addOrderDetail(OrderDetails $orderDetail): static
    {
        if (!$this->orderDetails->contains($orderDetail)) {
            $this->orderDetails->add($orderDetail);
            $orderDetail->setProcessor($this);
        }

        return $this;
    }

    public function removeOrderDetail(OrderDetails $orderDetail): static
    {
        if ($this->orderDetails->removeElement($orderDetail)) {
            // set the owning side to null (unless already changed)
            if ($orderDetail->getProcessor() === $this) {
                $orderDetail->setProcessor(null);
            }
        }

        return $this;
    }
}
