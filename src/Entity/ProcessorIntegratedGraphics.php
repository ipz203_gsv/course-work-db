<?php

namespace App\Entity;

use App\Repository\ProcessorIntegratedGraphicsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProcessorIntegratedGraphicsRepository::class)]
class ProcessorIntegratedGraphics
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
        private ?\DateTimeInterface $created = null;

    #[ORM\OneToMany(mappedBy: 'integrated_graphics_id', targetEntity: Processor::class)]
    private Collection $processor;

    #[ORM\OneToMany(mappedBy: 'integrated_graphics', targetEntity: Processor::class)]
    private Collection $processors;

    public function __construct()
    {
        $this->processor = new ArrayCollection();
        $this->processors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): static
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessor(): Collection
    {
        return $this->processor;
    }

    public function addProcessorDetail(Processor $processor): static
    {
        if (!$this->processor->contains($processor)) {
            $this->processor->add($processor);
            $processor->setIntegratedGraphics($this);
        }

        return $this;
    }

    public function removeProcessorDetail(Processor $processor): static
    {
        if ($this->processor->removeElement($processor)) {
            // set the owning side to null (unless already changed)
            if ($processor->getIntegratedGraphics() === $this) {
                $processor->setIntegratedGraphics(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Processor>
     */
    public function getProcessors(): Collection
    {
        return $this->processors;
    }

    public function addProcessor(Processor $processor): static
    {
        if (!$this->processors->contains($processor)) {
            $this->processors->add($processor);
            $processor->setIntegratedGraphics($this);
        }

        return $this;
    }

    public function removeProcessor(Processor $processor): static
    {
        if ($this->processors->removeElement($processor)) {
            // set the owning side to null (unless already changed)
            if ($processor->getIntegratedGraphics() === $this) {
                $processor->setIntegratedGraphics(null);
            }
        }

        return $this;
    }
}
