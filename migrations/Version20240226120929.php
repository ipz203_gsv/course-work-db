<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240226120929 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE processor_statistics (id INT AUTO_INCREMENT NOT NULL, processor_id INT NOT NULL, view_count INT NOT NULL, order_count INT NOT NULL, UNIQUE INDEX UNIQ_347CABAC37BAC19A (processor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE processor_statistics ADD CONSTRAINT FK_347CABAC37BAC19A FOREIGN KEY (processor_id) REFERENCES processor (id)');
        $this->addSql('ALTER TABLE customers CHANGE email email VARCHAR(180) NOT NULL, CHANGE roles roles JSON NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_62534E21E7927C74 ON customers (email)');
        $this->addSql('ALTER TABLE order_details CHANGE processor_id processor_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_details ADD CONSTRAINT FK_845CA2C137BAC19A FOREIGN KEY (processor_id) REFERENCES processor (id)');
        $this->addSql('CREATE INDEX IDX_845CA2C137BAC19A ON order_details (processor_id)');
        $this->addSql('ALTER TABLE processor CHANGE base_clock_speed base_clock_speed DOUBLE PRECISION NOT NULL, CHANGE max_clock_speed max_clock_speed DOUBLE PRECISION NOT NULL, CHANGE image image VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE reviews CHANGE created_at created_at DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE processor_statistics DROP FOREIGN KEY FK_347CABAC37BAC19A');
        $this->addSql('DROP TABLE processor_statistics');
        $this->addSql('DROP INDEX UNIQ_62534E21E7927C74 ON customers');
        $this->addSql('ALTER TABLE customers CHANGE email email VARCHAR(255) NOT NULL, CHANGE roles roles VARCHAR(255) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE order_details DROP FOREIGN KEY FK_845CA2C137BAC19A');
        $this->addSql('DROP INDEX IDX_845CA2C137BAC19A ON order_details');
        $this->addSql('ALTER TABLE order_details CHANGE processor_id processor_id INT NOT NULL');
        $this->addSql('ALTER TABLE processor CHANGE image image VARCHAR(255) DEFAULT NULL, CHANGE base_clock_speed base_clock_speed INT NOT NULL, CHANGE max_clock_speed max_clock_speed INT NOT NULL');
        $this->addSql('ALTER TABLE reviews CHANGE created_at created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }
}
